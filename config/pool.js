const { Pool } = require("pg");

const createPool = (props) => {
  const config = {
    host: "localhost",
    port: 5432,
    user: "developer",
    password: "dev",
    database: "assignment_fsw",
    ...props,
  };

  return new Pool(config);
};

module.exports = {
  createPool,
  query: (query, values) => createPool().query(query, values),
};
