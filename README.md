# Assigment Chapter 6 Topic 1

Creating Article Service

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/fsw-23-binar-academy/assigment-chapter-6-topic-1.git
git branch -M main
git push -uf origin main
```

## Create Database

```sql
`CREATE DATABASE assignment_fsw
    WITH
    OWNER = hanafiqp
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;`
```

## Create Table Users

```sql
`CREATE TABLE public.users
(
    id integer NOT NULL,
    username character varying(100),
    email character varying(150),
    password character varying(50),
    PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public.users
    OWNER to hanafiqp;`

sql`ALTER TABLE IF EXISTS public.users
    ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 );`
```

## Create Table Articles

```sql
`CREATE TABLE IF NOT EXISTS public.articles
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    title character varying(250) COLLATE pg_catalog."default",
    content text COLLATE pg_catalog."default",
    user_id integer,
    created_at date NOT NULL DEFAULT now(),
    updated_at date,
    CONSTRAINT articles_pkey PRIMARY KEY (id)
)`
```

## Create Table Comments

```sql
`CREATE TABLE IF NOT EXISTS public.comments
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    comment text COLLATE pg_catalog."default",
    user_id integer,
    article_id integer,
    created_at date NOT NULL DEFAULT now(),
    CONSTRAINT comments_pkey PRIMARY KEY (id)
)`
```

## Documentation Postman

[Check this out] (https://www.getpostman.com/collections/d2d9b0d4c93050a39876)
