var express = require("express");
var router = express.Router();

const { Pool, Client } = require("pg");

const pool = new Pool({
  user: "developer",
  host: "127.0.0.1",
  database: "assignment_fsw",
  password: "dev",
  port: 5432,
});

/* GET users listing. */
router.get("/", function (req, res, next) {
  pool.query(
    "select id, username, email, password from users",
    (err, response) => {
      if (!err) {
        res.json({
          data: response.rows,
        });
      } else {
        res.json({
          err,
          message: "error",
        });
      }
    }
  );
});

/* ADD users */
router.post("/", function (req, res, next) {
  const { username, email, password } = req.body;

  pool.query(
    "insert into users (username, email, password) values ($1, $2, $3) RETURNING *",
    [username, email, password],
    (err, response) => {
      if (!err) {
        console.log(response);
        res.json({
          data: response.rows,
          message: "success",
        });
      } else {
        res.json({
          err,
          message: "error",
        });
      }
    }
  );
});

/* UPDATE users */
router.put("/:id", function (req, res, next) {
  const { username, email, password } = req.body;
  const id = req.params.id;
  const query =
    "UPDATE users SET username = $1, email = $2, password = $3 WHERE id = $4 RETURNING *";
  const values = [username, email, password, id];
  pool.query(query, values, (err, response) => {
    if (!err) {
      res.json({
        data: response.rows,
        message: "success",
      });
    } else {
      res.json({
        err,
        message: "error",
      });
    }
  });
});

/* DELETE users */
router.delete("/:id", function (req, res, next) {
  const id = req.params.id;
  const query = "DELETE FROM users WHERE id = $1 RETURNING *";
  const values = [id];
  pool.query(query, values, (err, response) => {
    if (!err) {
      res.json({
        data: response.rows,
        message: "success",
      });
    } else {
      res.json({
        err,
        message: "error",
      });
    }
  });
});

/* GET users by id */
router.get("/:id", function (req, res, next) {
  const id = req.params.id;
  const query = "SELECT * FROM users WHERE id = $1";
  const values = [id];
  pool.query(query, values, (err, response) => {
    if (!err) {
      res.json({
        data: response.rows,
        message: "success",
      });
    } else {
      res.json({
        err,
        message: "error",
      });
    }
  });
});

module.exports = router;
