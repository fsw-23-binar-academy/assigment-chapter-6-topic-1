const express = require("express");
const router = express.Router();

const { createPool } = require("../config/pool");
const pool = createPool();

/* GET comments listing. */
router.get("/", function (req, res, next) {
  pool.query(
    "select comments.id, comment, users.username as creator, users.email as creator_mail from comments left join users on comments.user_id = users.id",
    (err, response) => {
      if (!err) {
        res.json({
          data: response.rows,
        });
      } else {
        res.json({
          err,
          message: "error",
        });
      }
    }
  );
});

/* ADD comments */
router.post("/", function (req, res, next) {
  const { comment, article_id, user_id } = req.body;

  pool.query(
    "insert into comments (comment, article_id, user_id) values ($1, $2, $3) RETURNING *",
    [comment, article_id, user_id],
    (err, response) => {
      if (!err) {
        res.json({
          data: response.rows,
          message: "success",
        });
      } else {
        res.json({
          err,
          message: "error",
        });
      }
    }
  );
});

/* UPDATE comments */
router.put("/:id", function (req, res, next) {
  const { comment } = req.body;
  const id = req.params.id;
  const query = "UPDATE comments SET comment = $1 WHERE id = $2 RETURNING *";
  const values = [comment, id];
  pool.query(query, values, (err, response) => {
    if (!err) {
      res.json({
        data: response.rows,
        message: "success",
      });
    } else {
      res.json({
        err,
        message: "error",
      });
    }
  });
});

/* DELETE comments */
router.delete("/:id", function (req, res, next) {
  const id = req.params.id;
  const query = "DELETE FROM comments WHERE id = $1 RETURNING *";
  const values = [id];
  pool.query(query, values, (err, response) => {
    if (!err) {
      res.json({
        data: response.rows,
        message: "success",
      });
    } else {
      res.json({
        err,
        message: "error",
      });
    }
  });
});

/* GET comments by id */
router.get("/:id", function (req, res, next) {
  const id = req.params.id;
  const query =
    "SELECT comments.*, users.username as author, users.email as author_mail from comments left join users on comments.user_id = users.id WHERE comments.id = $1";
  const values = [id];
  pool.query(query, values, (err, response) => {
    if (!err) {
      res.json({
        data: response.rows,
        message: "success",
      });
    } else {
      res.json({
        err,
        message: "error",
      });
    }
  });
});

module.exports = router;
