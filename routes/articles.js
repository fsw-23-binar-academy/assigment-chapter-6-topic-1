const express = require("express");
const router = express.Router();

const { createPool, query } = require("../config/pool");
const pool = createPool();

module.exports = router;

/* GET articles listing. */
router.get("/", function (req, res, next) {
  pool.query(
    "select articles.id, title, content, users.username as author, users.email as author_mail from articles left join users on articles.user_id = users.id",
    (err, response) => {
      if (!err) {
        res.json({
          data: response.rows,
        });
      } else {
        res.json({
          err,
          message: "error",
        });
      }
    }
  );
});

/* ADD articles */
router.post("/", function (req, res, next) {
  const { title, content, user_id } = req.body;

  pool.query(
    "insert into articles (title, content, user_id) values ($1, $2, $3) RETURNING *",
    [title, content, user_id],
    (err, response) => {
      if (!err) {
        res.json({
          data: response.rows,
          message: "success",
        });
      } else {
        res.json({
          err,
          message: "error",
        });
      }
    }
  );
});

/* UPDATE articles */
router.put("/:id", function (req, res, next) {
  const { title, content } = req.body;
  const id = req.params.id;
  const query =
    "UPDATE articles SET title = $1, content = $2 WHERE id = $3 RETURNING *";
  const values = [title, content, id];
  pool.query(query, values, (err, response) => {
    if (!err) {
      res.json({
        data: response.rows,
        message: "success",
      });
    } else {
      res.json({
        err,
        message: "error",
      });
    }
  });
});

/* DELETE articles */
router.delete("/:id", function (req, res, next) {
  const id = req.params.id;
  const query = "DELETE FROM articles WHERE id = $1 RETURNING *";
  const values = [id];
  pool.query(query, values, (err, response) => {
    if (!err) {
      res.json({
        data: response.rows,
        message: "success",
      });
    } else {
      res.json({
        err,
        message: "error",
      });
    }
  });
});

/* GET article with comments listing. */
router.get("/with-comments", async (req, res, next) => {
  const query_articles =
    "select articles.id, title, content, users.username as author, users.email as author_mail from articles left join users on articles.user_id = users.id";

  const query_comments =
    "select comments.*, users.username as creator, users.email as creator_mail from comments left join users on comments.user_id = users.id ";

  const articles = await query(query_articles).then(
    (response) => response.rows
  );

  const comments = await query(query_comments).then(
    (response) => response.rows
  );

  articles.map(
    (article) => {
      article.comments = comments.filter((comment) => {
        return comment.article_id === article.id;
      });
    } // end of map
  ); // end of articles.map

  res.json({
    data: articles,
    message: "success",
  });
});

/* GET articles by id */
router.get("/:id", function (req, res, next) {
  const id = req.params.id;
  const query =
    "SELECT articles.*, users.username as author, users.email as author_mail from articles left join users on articles.user_id = users.id WHERE articles.id = $1";
  const values = [id];
  pool.query(query, values, (err, response) => {
    if (!err) {
      res.json({
        data: response.rows,
        message: "success",
      });
    } else {
      res.json({
        err,
        message: "error",
      });
    }
  });
});
